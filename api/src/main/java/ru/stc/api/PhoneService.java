package ru.stc.api;

public interface PhoneService {

    String getPhone();

    Person getPerson();
}
